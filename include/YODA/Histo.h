// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2020 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_Histo_h
#define YODA_Histo_h

#include "YODA/AnalysisObject.h"
#include "YODA/Exceptions.h"

namespace YODA {


  /// A base class for all histograms
  class Histo : public AnalysisObject {
  public:

    /// @name Constructors
    //@{

    /// Virtual destructor for inheritance
    virtual ~Histo1D() { }


    /// Fill-dimension of this data object
    virtual size_t dim() const = 0;


    /// @name Modifiers
    //@{

    /// @brief Reset the histogram.
    ///
    /// Keep the binning but set all bin contents and related quantities to zero
    virtual void reset() = 0;

    /// Fill histo bin @a i with the given weight, optionally as a fractional fill
    virtual void fillBin(size_t i, double weight=1.0, double fraction=1.0) = 0;


    /// Rescale as if all fill weights had been different by factor @a scalefactor
    virtual void scaleW(double scalefactor) = 0;
    // setAnnotation("ScaledBy", annotation<double>("ScaledBy", 1.0) * scalefactor);


    /// @todo Makes sense for differential histos but not profiles: discriminate the types?
    // /// Normalize the (visible) histo area to the @a normto value.
    // ///
    // /// If @a includeoverflows is true, the original normalisation is computed with
    // /// the overflow bins included, so that the resulting visible normalisation can
    // /// be less than @a normto. This is probably what you want.
    // virtual void normalize(double normto=1.0, bool includeoverflows=true);



    /// @todo These make sense in 1D; less clear about other dimensionalities
    ///
    // /// Merge every group of n bins, starting from the LHS
    // void rebinBy(unsigned int n, size_t begin=0, size_t end=UINT_MAX) {
    //   _axis.rebinBy(n, begin, end);
    // }
    // /// Overloaded alias for rebinBy
    // void rebin(unsigned int n, size_t begin=0, size_t end=UINT_MAX) {
    //   rebinBy(n, begin, end);
    // }

    // /// Rebin to the given list of bin edges
    // void rebinTo(const std::vector<double>& newedges) {
    //   _axis.rebinTo(newedges);
    // }
    // /// Overloaded alias for rebinTo
    // void rebin(const std::vector<double>& newedges) {
    //   rebinTo(newedges);
    // }


  public:

    /// @name Bin accessors
    /// @{

    /// Number of bins (not counting under/overflow)
    virtual size_t numBins() const = 0;

    /// Check if binning is the same as different Histo1D
    ///
    /// @todo Needs RTTI for inequivalent types
    // bool sameBinning(const Histo& h) {
    //   return _axis == h1._axis;
    // }

    /// @}


    /// @name Bin adding and removal
    /// @{

    // /// Add a new bin specifying its lower and upper bound
    // void addBin(double from, double to) { _axis.addBin(from, to); }

    // /// Add new bins by specifying a vector of edges
    // void addBins(std::vector<double> edges) { _axis.addBins(edges); }

    // // /// Add new bins specifying a beginning and end of each of them
    // // void addBins(std::vector<std::pair<double,double> > edges) {
    // //   _axis.addBins(edges);
    // // }

    // /// Add a new bin, perhaps already populated: CAREFUL!
    // void addBin(const HistoBin1D& b) { _axis.addBin(b); }

    // /// @brief Bins addition operator
    // ///
    // /// Add multiple bins without resetting
    // void addBins(const Bins& bins) {
    //   _axis.addBins(bins);
    // }

    /// Remove a bin
    virtual void rmBin(size_t index) = 0;

    /// Remove several bins
    ///
    /// @todo Implement, cf. Scatter::rmPoints()
    // virtual void rmBins(size_t index) = 0;

    /// @deprecated Use rmBin
    void eraseBin(size_t index) { rmBin(index); }

    //@}


    /// @name Whole histo data
    //@{

    /// Get the total area (sumW) of the histogram
    // double integral(bool includeoverflows=true) const { return sumW(includeoverflows); }

    /// Get the number of fills
    virtual double numEntries(bool includeoverflows=true) const = 0;

    /// Get the effective number of fills
    virtual double effNumEntries(bool includeoverflows=true) const = 0;

    /// Get sum of weights in histo
    virtual double sumW(bool includeoverflows=true) const = 0;

    /// Get sum of squared weights in histo
    virtual double sumW2(bool includeoverflows=true) const = 0;

    //@}

  };


}

#endif
